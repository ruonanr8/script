
#!/bin/bash
trc_dir=/u01/app/oracle/diag/rdbms/orcl/orcl2/trace
for file_name in ${trc_dir}/*
do
    temp_file=`basename ${file_name}`
    file_size=`du -sm ${trc_dir}/${temp_file}|awk '{print $1}'`
       if [ ${file_size} > "20" ]; then
           echo '0' > ${trc_dir}/${temp_file}
       fi
done
